package io.bitcarpentry.memory;

public class FirstLettersProcessor {

    public String process(String input) {
        if (input == null) {
            throw new IllegalArgumentException();
        }
        StringBuilder output = new StringBuilder();
        boolean firstLetter = true;
        for (char c : input.toCharArray()) {
            if (Character.isLetter(c)) {
                if (firstLetter) {
                    firstLetter = false;
                    output.append(c);
                } else {
                    output.append('_');
                }
            } else {
                firstLetter = true;
                output.append(c);
            }
        }
        return output.toString();
    }

}
