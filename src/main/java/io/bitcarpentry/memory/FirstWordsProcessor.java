package io.bitcarpentry.memory;

import java.util.Scanner;
import java.util.StringTokenizer;

public class FirstWordsProcessor {

  private final static int MAX_WORDS = 2;

  public String process(String input) {
    if (input == null) {
      throw new IllegalArgumentException();
    }
    StringBuilder output = new StringBuilder();
    Scanner scanner = new Scanner(input);
    while (scanner.hasNextLine()) {
      StringTokenizer tokenizer = new StringTokenizer(scanner.nextLine());
      int wordCount = MAX_WORDS;
      while (tokenizer.hasMoreTokens() && wordCount > 0) {
        output.append(tokenizer.nextToken());
        output.append(' ');
        wordCount--;
      }
      output.deleteCharAt(output.length() - 1);
      output.append('\n');
    }
    output.deleteCharAt(output.length() - 1);
    return output.toString();
  }

}
