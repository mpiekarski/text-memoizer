package io.bitcarpentry.memory

import spock.lang.Specification
import spock.lang.Subject

class FirstWordsProcessorSpec extends Specification {

    @Subject
    FirstWordsProcessor processor = new FirstWordsProcessor()

    def "should process text leaving only first words of sentences"() {
        given:
            String text = '''
                Jako wszytki narody Rzymowi służyły,
                Póki mu dostawało i szczęścia, i siły,
                Także też, skoro mu się powinęła noga,
                Ze wszytkiego nań świata uderzyła trwoga.
                Fortunniejszy był język, bo ten i dziś miły:
                Tak zawżdy trwalszy owoc dowcipu niż siły.
            '''.stripIndent().trim()
        when:
            String processedText = processor.process(text)
        then:
            processedText == '''
                Jako wszytki
                Póki mu
                Także też,
                Ze wszytkiego
                Fortunniejszy był
                Tak zawżdy
            '''.stripIndent().trim()
    }

    def "should throw IllegalArgumentException for null input"() {
        when:
            processor.process(null)
        then:
            thrown(IllegalArgumentException)
    }

}
