package io.bitcarpentry.memory

import spock.lang.Specification
import spock.lang.Subject

class FirstLettersProcessorSpec extends Specification {

    @Subject
    FirstLettersProcessor processor = new FirstLettersProcessor()

    def "should process text leaving only first letters of words"() {
        given:
            String text = '''
                Jako wszytki narody Rzymowi służyły,
                Póki mu dostawało i szczęścia, i siły,
                Także też, skoro mu się powinęła noga,
                Ze wszytkiego nań świata uderzyła trwoga.
                Fortunniejszy był język, bo ten i dziś miły:
                Tak zawżdy trwalszy owoc dowcipu niż siły.
            '''.stripIndent().trim()
        when:
            String processedText = processor.process(text)
        then:
            processedText == '''
                J___ w______ n_____ R______ s______,
                P___ m_ d________ i s________, i s___,
                T____ t__, s____ m_ s__ p_______ n___,
                Z_ w_________ n__ ś_____ u_______ t_____.
                F____________ b__ j____, b_ t__ i d___ m___:
                T__ z_____ t_______ o___ d______ n__ s___.
            '''.stripIndent().trim()
    }

    def "should throw IllegalArgumentException for null input"() {
        when:
            processor.process(null)
        then:
            thrown(IllegalArgumentException)
    }

}
